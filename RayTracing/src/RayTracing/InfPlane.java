package RayTracing;

public class InfPlane extends Shape {
	private double offset;
	private Vector3D normal;
	// plane: all P s.t. P*normal = offset
	public InfPlane(int material_id, Vector3D normal, double offset) {
		super(material_id);
		this.normal = normal;
		this.offset = offset;
	}
	
	public void setOffset(double offset){
		this.offset = offset;
	}
	public Vector3D getNormal(){
		return this.normal;
	}
	public void setNormal(Vector3D normal){
		this.normal = normal;
	}
	
	
	public double getIntersection(Ray ray) {
		double div = ray.getDirection().dot(normal);
		if (Utils.isZero(div)){ // parallel			
			return Shape.NO_INTERSECTION;
		}
		
		double t = -(ray.getPoint().dot(normal) - offset) / div;
		
		if (t < 0){ // plane is "behind" camera
			return Shape.NO_INTERSECTION;
		}
		return t;
	}

	
	@Override
	public String toString() {
		return "InfPlane [offset=" + offset + ", normal=" + normal
				+ ", getMaterial()=" + getMaterial() + "]";
	}
		
	public String toEqString() {
		return normal.getX() + "*x + " + normal.getY() + "*y + " + normal.getZ() + "*z = " + offset;
	}

	@Override
	public Vector3D getSurfaceNormal(Vector3D surfacePoint) {
		// Notice that this is the normal pointing in the direction the plane is facing ('out').
		return this.normal.normalize();
	}
	
	public boolean isPointOnPlane(Vector3D p){
		return Utils.isZero(p.dot(normal) - offset);
	}

	public double getOffset() {
		return offset;
	}

}
