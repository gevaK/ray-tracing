package RayTracing;

public class Camera {
	
	private Vector3D position;
	private Vector3D lookAt;
	private Vector3D direction; // Direction the camera is facing
	private Vector3D upVector; // Normalized vector looking upward, perpendicular to lookAt 
	private Vector3D rightVector; // Normalized vector pointing right, obtained by vector product of lookAt and upVector
	private double screenDistance;
	private double screenWidth;
	private double screenHeight; // Height of the screen. Calculated by screenWidth and image aspect ratio.
	//private Vector3D screenBotLeft; // Bottom-left corner of the screen
	
	public Camera() {
		this.position = null;
		this.lookAt = null;
		this.direction = null;
		this.upVector = null;
		this.rightVector = null;
		this.screenDistance = Scene.VALUE_UNINITIALIZED;
		this.screenWidth = Scene.VALUE_UNINITIALIZED;
		this.screenHeight = Scene.VALUE_UNINITIALIZED;
	}
	
	/**
	 * Gets (x, y) position on the screen, both in the range [0, 1].
	 * Returns a ray from the camera to the given position on the screen.
	 */
	public Ray getRayToScreenPosition(double x, double y) {
		// Find screen bottom left
		Vector3D screenMiddle =  this.position.add(direction.scale(this.screenDistance));
		Vector3D screenBotLeft = screenMiddle.sub(this.rightVector.scale(screenWidth / 2)).sub(this.upVector.scale(screenHeight / 2));
		// Calculate location of the hit point on screen
		double xPos = x * screenWidth;
		double yPos = y * screenHeight;
		Vector3D rightOffset = rightVector.scale(xPos);
		Vector3D upOffset = upVector.scale(yPos);
		Vector3D rayDir = screenBotLeft.add(rightOffset).add(upOffset).sub(this.position);
		// Create the ray object
		return new Ray(this.position, rayDir); 
	}

	public Vector3D getPosition() {
		return position;
	}

	/**
	 * Sets the camera's position and orientation, by the point it's looking at and the 'up' direction.
	 * @param position
	 * @param lookAt
	 * @param upVector
	 */
	public void setPose(Vector3D position, Vector3D lookAt, Vector3D upVector) {
		this.position = position;
		this.lookAt = lookAt;
		this.direction = this.lookAt.sub(this.position).normalize();
		// Make upVector orthogonal to direction
		this.upVector = upVector.sub(upVector.project(direction));
		this.upVector = this.upVector.normalize();
		assert Utils.isZero(this.upVector.dot(direction)); 
		// Set right vector
		this.rightVector = direction.cross(upVector).normalize();
		assert Utils.isZero(this.rightVector.getSquareNorm() - 1);
	}
	
	/**
	 * Sets the screen parameters for the camera.
	 * @param aspectRatio width / height proportion
	 */
	public void setScreenParams(double screenDistance, double screenWidth, double aspectRatio) {
		this.screenDistance = screenDistance;
		this.screenWidth = screenWidth;
		this.screenHeight = screenWidth / aspectRatio;
	}
	
	/**
	 * Makes sure this object was initialized properly.
	 * @throws RayTracer.RayTracerException if we weren't initialized properly. The error message describes the reason.
	 */
	public void verifyInitialization() throws RayTracer.RayTracerException {
		if (this.position == null || this.lookAt == null || this.direction == null || this.upVector == null || this.rightVector == null)
			throw new RayTracer.RayTracerException("Initialization Error: camera pose uninitialized.");
		if (this.screenDistance == Scene.VALUE_UNINITIALIZED || this.screenHeight == Scene.VALUE_UNINITIALIZED || this.screenWidth == Scene.VALUE_UNINITIALIZED)
			throw new RayTracer.RayTracerException("Initialization Error: camera screen parameters uninitialized.");
	}
}
