package RayTracing;


/**
 * This class represents a color, separated to red, green and blue components, each valued in range [0, 1].
 */
public class Color {
	
	private double red;
	private double green;
	private double blue;
	
	public static Color BLACK = new Color();
	public static Color RED = new Color(1, 0, 0);
	public static Color GREEN = new Color(0, 1, 0);
	public static Color BLUE = new Color(0, 0, 1);
	public static Color WHITE = new Color(1,1,1);
	
	/**
	 * Empty constructor - returns black color.
	 */
	public Color() {
		this(0, 0, 0);
	}
	
	public Color(double red, double green, double blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}
	
	/**
	 * Returns the red component strength.
	 * Notice that while the internal values can be larger than 1 due to color additions, 
	 * the value returned from this method will always be cut-off to be at most 1.
	 */
	public double getRed() {
		return Math.max(0, Math.min(red, 1));
	}
	
	/**
	 * Returns the green component strength.
	 * Notice that while the internal values can be larger than 1 due to color additions, 
	 * the value returned from this method will always be cut-off to be at most 1.
	 */
	public double getGreen() {
		return Math.max(0, Math.min(green, 1));
	}

	/**
	 * Returns the blue component strength.
	 * Notice that while the internal values can be larger than 1 due to color additions, 
	 * the value returned from this method will always be cut-off to be at most 1.
	 */
	public double getBlue() {
		return Math.max(0, Math.min(blue, 1));
	}
	
	/**
	 * Color addition (overlay).
	 * Sums the different components and cuts off anything over 1.
	 */
	public Color add(Color c) {
		double red = this.red + c.red; 
		double green = this.green + c.green;
		double blue = this.blue + c.blue;
		return new Color(red, green, blue);
	}


	/**
	 * Color scaling.
	 * Usually used with scalar between 0 and 1.
	 */
	public Color scale(double scalar) {
		double red = this.red * scalar; 
		double green = this.green * scalar;
		double blue = this.blue * scalar;
		return new Color(red, green, blue);
	}
	
	/**
	 * Color multiplication, done independently for each component.
	 */
	public Color multiply(Color c) {
		double red = this.red * c.red; 
		double green = this.green * c.green;
		double blue = this.blue * c.blue;
		return new Color(red, green, blue);
	}
	
	public String toString() {
		int red = (int)(this.red * 255);
		int green = (int)(this.green * 255);
		int blue = (int)(this.blue * 255);
		return "rgb(" + String.valueOf(red) + ", " +  String.valueOf(green) + ", " + String.valueOf(blue) + ")";
	}
}
