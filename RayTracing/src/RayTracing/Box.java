package RayTracing;
import java.util.Arrays;

public class Box extends Shape {
	//private Vector3D center;
	//private double scaleX, scaleY, scaleZ, rotationX, rotationY, rotationZ; 
	private InfPlane px1, px2, py1, py2, pz1, pz2;
	private SortableBoxEntry[] entry_arr;
	
	/*
	 * expects rotations in degrees (to match input files. maybe change to radians?)
	 */
	public Box(int material_id, Vector3D center, double scaleX, double scaleY, double scaleZ, double rotationX, double rotationY, double rotationZ) {				
		super(material_id);
		// start with 6 planes aligned around (0,0,0)
		scaleX = scaleX / 2;
		scaleY = scaleY / 2;
		scaleZ = scaleZ / 2;
		
		double rx = Math.toRadians(rotationX);
		double ry = Math.toRadians(rotationY);
		double rz = Math.toRadians(rotationZ);

		// calculate the rotated normals for each plane
		Vector3D nx = (new Vector3D(1, 0, 0)).rotate(rx, ry, rz);
		Vector3D ny = (new Vector3D(0, 1, 0)).rotate(rx, ry, rz);
		Vector3D nz = (new Vector3D(0, 0, 1)).rotate(rx, ry, rz);
	
		// calculate a point on each plane by rotating where it used to be, and moving it to be around the center
		// optimization: (scaleX, 0, 0).rotate(rx, ry, rz) == nx.scale(scaleX), so we don't need to calculate again. 
		Vector3D pointx1 = nx.scale(scaleX).add(center);
		Vector3D pointx2 = nx.scale(-scaleX).add(center);
		Vector3D pointy1 = ny.scale(scaleY).add(center);
		Vector3D pointy2 = ny.scale(-scaleY).add(center);
		Vector3D pointz1 = nz.scale(scaleZ).add(center);
		Vector3D pointz2 = nz.scale(-scaleZ).add(center);
			
		// create each plane by its normal and offset (calculated from a point on it)
		this.px1 = new InfPlane(material_id, new Vector3D(nx), pointx1.dot(nx));
		this.px2 = new InfPlane(material_id, new Vector3D(nx), pointx2.dot(nx));

		this.py1 = new InfPlane(material_id, new Vector3D(ny), pointy1.dot(ny));
		this.py2 = new InfPlane(material_id, new Vector3D(ny), pointy2.dot(ny));
		
		this.pz1 = new InfPlane(material_id, new Vector3D(nz), pointz1.dot(nz));
		this.pz2 = new InfPlane(material_id, new Vector3D(nz), pointz2.dot(nz));
				
		// initialize entry_arr to be used later
		entry_arr = new SortableBoxEntry[6];
		entry_arr[0] = new SortableBoxEntry(0, px1, py1, py2, pz1, pz2, 0);
		entry_arr[1] = new SortableBoxEntry(0, px2, py1, py2, pz1, pz2, 1);
		entry_arr[2] = new SortableBoxEntry(0, py1, px1, px2, pz1, pz2, 2);
		entry_arr[3] = new SortableBoxEntry(0, py2, px1, px2, pz1, pz2, 3);
		entry_arr[4] = new SortableBoxEntry(0, pz1, px1, px2, py1, py2, 4);
		entry_arr[5] = new SortableBoxEntry(0, pz2, px1, px2, py1, py2, 5);	
	}
	
	@Override
	public double getIntersection(Ray ray) {
		// for each plane bounding the box:
		// update its distance from its matching plane	
		for (int i=0; i<6; i++)
			entry_arr[i].point = entry_arr[i].p.getIntersection(ray);

		Arrays.sort(entry_arr); // sort by distance on ray
		
		Vector3D p;
		double o1, o2;
		for (SortableBoxEntry e : entry_arr){ // iterate from closest intersection to farthest						
			if (e.point == Shape.NO_INTERSECTION) // if no intersection - no need to look at other planes.				
				return Shape.NO_INTERSECTION;			
			
			// found a ray intersecting with a plane. need to check if it is between the 2 other pairs of planes
			p = ray.getPointAtOffset(e.point); // get the point of intersection
			
			o1 = e.i1.getNormal().dot(p); // get the offset of this point on the two bounding normals
			o2 = e.j1.getNormal().dot(p);

			// if the offset of the point is between the offsets of both bounding planes - it is a valid intersection
			if (o1 < Math.max(e.i1.getOffset(), e.i2.getOffset()) &&
				o1 > Math.min(e.i1.getOffset(), e.i2.getOffset()) &&
				o2 < Math.max(e.j1.getOffset(), e.j2.getOffset()) &&
				o2 > Math.min(e.j1.getOffset(), e.j2.getOffset()))				
				return e.point;			
		}
		// default, found no intersections
		return Shape.NO_INTERSECTION;
	}
	
	@Override
	public Vector3D getSurfaceNormal(Vector3D surfacePoint) {
		// iterate over each plane. if the point is on the plane - return the normal of the plane pointed OUTWARDS.
		if (px1.isPointOnPlane(surfacePoint)){
			if (px1.getOffset() > px2.getOffset())
				return px1.getSurfaceNormal(surfacePoint);
			else
				return px1.getSurfaceNormal(surfacePoint).scale(-1);
		}

		if (px2.isPointOnPlane(surfacePoint)){
			if (px2.getOffset() > px1.getOffset())
				return px2.getSurfaceNormal(surfacePoint);
			else
				return px2.getSurfaceNormal(surfacePoint).scale(-1);
		}

		if (py1.isPointOnPlane(surfacePoint)){
			if (py1.getOffset() > py2.getOffset())
				return py1.getSurfaceNormal(surfacePoint);
			else
				return py1.getSurfaceNormal(surfacePoint).scale(-1);
		}

		if (py2.isPointOnPlane(surfacePoint)){
			if (py2.getOffset() > py1.getOffset())
				return py2.getSurfaceNormal(surfacePoint);
			else
				return py2.getSurfaceNormal(surfacePoint).scale(-1);
		}

		if (pz1.isPointOnPlane(surfacePoint)){
			if (pz1.getOffset() > pz2.getOffset())
				return pz1.getSurfaceNormal(surfacePoint);
			else
				return pz1.getSurfaceNormal(surfacePoint).scale(-1);
		}

		if (pz2.isPointOnPlane(surfacePoint)){
			if (pz2.getOffset() > pz1.getOffset())
				return pz2.getSurfaceNormal(surfacePoint);
			else
				return pz2.getSurfaceNormal(surfacePoint).scale(-1);
		}
		throw new RuntimeException();		
	}
	
	@Override
	public String toString() {
		return "Box\n[px1 = " + px1.toEqString() + ",\n px2 = " + px2.toEqString() + ",\n py1 = " + py1.toEqString() + ",\n py2 = " + py2.toEqString() + ",\n pz1 = " + pz1.toEqString() + ",\n pz2 = " + pz2.toEqString() + "]";
	}

}
