package RayTracing;

public class Vector3D {
	
	private double x, y, z;
	
	public static Vector3D ZERO = new Vector3D();
	
	/**
	 * Empty constructor for creating a zero vector.
	 */
	public Vector3D() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}
	
	/**
	 * Copy constructor.
	 */
	public Vector3D(Vector3D vec) {
		this.x = vec.x;
		this.y = vec.y;
		this.z = vec.z;
	}
	
	/**
	 * Construct vector by 3 double coordinates.
	 */
	public Vector3D(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Vector addition.
	 */
	public Vector3D add(Vector3D vec) {
		return new Vector3D(this.x + vec.x, this.y+vec.y, this.z+vec.z);
	}
	
	/**
	 * Vector scalar multiplication (scale).
	 */
	public Vector3D scale(double scalar) {
		return new Vector3D(scalar * this.x, scalar * this.y, scalar * this.z);
	}
	
	/**
	 * Vector substraction.
	 */
	public Vector3D sub(Vector3D vec) {
		return this.add(vec.scale(-1));
	}
	
	/**
	 * Vector dot product.
	 */
	public double dot(Vector3D vec) {
		return this.x * vec.x + this.y * vec.y + this.z * vec.z;
	}
	
	public Vector3D cross(Vector3D vec) {
		double x = this.y * vec.z - this.z * vec.y;
		double y = this.z * vec.x - this.x * vec.z;
		double z = this.x * vec.y - this.y * vec.x;
		Vector3D result = new Vector3D(x, y, z);
		assert Utils.isZero(result.dot(this));
		assert Utils.isZero(result.dot(vec)); 
		return result;
	}
	
	/**
	 * Returns the norm of this vector, squared.
	 * More efficient than calculating the norm.
	 */
	public double getSquareNorm() {
		return this.dot(this);
	}
	
	/**
	 * Returns the norm of this vector.
	 * Relatively costly because it performs sqrt.
	 */
	public double getNorm() {
		return Math.sqrt(this.getSquareNorm());
	}
	
	/**
	 * Returns a normalized version of this vector.
	 * Does nothing in case this is the zero vector.
	 */
	public Vector3D normalize() {
		double norm = this.getNorm();
		if (norm == 0) 
			return this;
		else
			return this.scale(1 / norm);
	}
	
	/**
	 * Returns the projection of this vector on axis 'vec'.
	 * Returns zero vector if 'vec' is the zero vector.
	 */
	public Vector3D project(Vector3D vec) {
		return vec.scale(this.dot(vec) / vec.getSquareNorm());
	
	}
	
	public boolean equals(Vector3D v2){
		// maybe check for normalized vectors
		return (this.x == v2.getX() && this.y == v2.getY() && this.z == v2.getZ());
	}
	
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;
	}
	public double getZ(){
		return this.z;
	}
	
	public String toString(){
		return "{X: " + x + ", Y: " + y + ", Z: " + z + "}";
	}
	
	public Vector3D rotate(double radiansX, double radiansY, double radiansZ){
		double x = this.x;
		double y = this.y;
		double z = this.z;
		double sintX = Math.sin(radiansX);
		double costX = Math.cos(radiansX);
		double sintY = Math.sin(radiansY);
		double costY = Math.cos(radiansY);
		double sintZ = Math.sin(radiansZ);
		double costZ = Math.cos(radiansZ);
		double tmp;
		// rotate x
		tmp = y*costX - z*sintX;
		z = y*sintX + z*costX;
		y = tmp;	
		
		// rotate y
		tmp = x*costY + z*sintY;
		z = -x*sintY + z*costY;
		x = tmp;
		
		// rotate z
		tmp = x*costZ - y*sintZ;
		y = x*sintZ + y*costZ;
		x = tmp;		
		return new Vector3D(x, y, z);
	}
	
	/**
	 * Returns an arbitrary vector that's orthogonal to this vector.
	 * If we got the zero vector, return zero vector.
	 */
	public Vector3D getOrthogonalVector() {
		Vector3D vec; // arbitrary vector not parallel to this one
		if (!Utils.isZero(x) || !Utils.isZero(y)) {
			vec = new Vector3D(0, 0, 1);
		} else if (!Utils.isZero(z)) {
			vec = new Vector3D(1, 0, 0);
		} else {
			//assert false; // We should never reach here during testing
			return Vector3D.ZERO;
		}
		Vector3D perp = this.cross(vec); // Pick orthogonal vector by cross product
		return perp;
	}
}
