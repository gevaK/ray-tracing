package RayTracing;

public class Sphere extends Shape {
	private Vector3D center;
	private double r;

	public Sphere(int material_id, Vector3D center, double r) {
		super(material_id);
		this.setCenter(center);
		this.setR(r);		
	}

	public Vector3D getCenter() {
		return center;
	}

	public void setCenter(Vector3D center) {
		this.center = center;
	}

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}

	@Override
	public double getIntersection(Ray ray) {
		Vector3D d = ray.getDirection(); // direction
		Vector3D s = ray.getPoint(); // source point

		// calculate intersection point by solving a quadratic equation with the following params:
		double A = d.getSquareNorm();
		double B = 2 * (d.dot(s) - d.dot(center));
		double C = s.sub(center).getSquareNorm() - Math.pow(r, 2);
		double det = B * B - 4 * A * C; 
		
		if (det < 0){
			return Shape.NO_INTERSECTION;
		}
		
		else if (det == 0){
			return -B / (2 * A);
		}
		
		// calculate both roots
		double t0 = (-B - Math.sqrt(det)) / (2 * A);
		double t1 = (-B + Math.sqrt(det)) / (2 * A);
		
		if (t0 < 0 && t1 < 0){
			// both intersection points are "behind" the camera
			return Shape.NO_INTERSECTION;
		}
		
		else if (t0 < 0 && t1 > 0){
			// the second intersection point is in front of us. we are "inside" the sphere. 
			return t1;
		}
		
		else if (t0 > 0 && t1 > t0){
			// return the first intersection point
			return t0;
		}
		// we shouldn't reach this
		throw new RuntimeException();		
	}

	@Override
	public String toString() {
		return "Sphere [center=" + center + ", r=" + r + ", getMaterial()="
				+ getMaterial() + "]";
	}

	@Override
	public Vector3D getSurfaceNormal(Vector3D surfacePoint) {
		Vector3D vec = surfacePoint.sub(this.center);
		return vec.normalize();
	}

}
