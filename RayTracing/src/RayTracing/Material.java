package RayTracing;

public class Material {
	private Color diffuseColor;
	private Color specularColor;
	private Color reflectionColor;
	private int phongCoefficient;
	private double transparency;
	
	public Material() {
		this.diffuseColor = null;
		this.specularColor = null;
		this.reflectionColor = null;
		this.phongCoefficient = Scene.VALUE_UNINITIALIZED;
		this.transparency = Scene.VALUE_UNINITIALIZED;
	}
	
	public Color getDiffuseColor() {
		return diffuseColor;
	}
	
	public void setDiffuseColor(Color diffuseColor) {
		this.diffuseColor = diffuseColor;
	}
	
	public Color getSpecularColor() {
		return specularColor;
	}
	
	public void setSpecularColor(Color specularColor) {
		this.specularColor = specularColor;
	}
	
	public int getPhongCoefficient() {
		return phongCoefficient;
	}
	
	public void setPhongCoefficient(int phongCoefficient) {
		this.phongCoefficient = phongCoefficient;
	}
	
	public Color getReflectionColor() {
		return reflectionColor;
	}
	
	public void setReflectionColor(Color reflectionColor) {
		this.reflectionColor = reflectionColor;
	}
	
	public double getTransparency() {
		return transparency;
	}
	
	public void setTransparency(double tranparency) {
		this.transparency = tranparency;
	}
	
	public boolean isReflective() {
		return (this.reflectionColor != Color.BLACK);
	}
	
	public boolean isTransparent() {
		return (this.transparency != 0);
	}

	@Override
	public String toString() {
		return "Material [diffuseColor=" + diffuseColor + ", specularColor="
				+ specularColor + ", phongCoefficient=" + phongCoefficient
				+ ", reflectionColor=" + reflectionColor + ", transparency="
				+ transparency + "]";
	}
	
	/**
	 * Makes sure this object was initialized properly.
	 * @throws RayTracer.RayTracerException if we weren't initialized properly. The error message describes the reason.
	 */
	public void verifyInitialization() throws RayTracer.RayTracerException {
		if (this.diffuseColor == null)
			throw new RayTracer.RayTracerException("Initialization Error: material diffuse color uninitialized.");
		if (this.specularColor == null)
			throw new RayTracer.RayTracerException("Initialization Error: material specular color uninitialized.");
		if (this.reflectionColor == null)
			throw new RayTracer.RayTracerException("Initialization Error: material reflection color uninitialized.");
		if (this.phongCoefficient == Scene.VALUE_UNINITIALIZED)
			throw new RayTracer.RayTracerException("Initialization Error: material phong coefficient uninitialized.");
		if (this.transparency == Scene.VALUE_UNINITIALIZED)
			throw new RayTracer.RayTracerException("Initialization Error: material transparency uninitialized.");
	}
	
}
