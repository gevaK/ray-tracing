package RayTracing;

public abstract class Shape {
	
	public static double NO_INTERSECTION = Double.POSITIVE_INFINITY;
	
	private Material material;
	private int materialId;
	
	public Shape(int materialId) {
		this.material = null; // Material is initialized by the scene after all materials were loaded
		this.materialId = materialId;
	}
	
	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	/**
	 * Checks intersection with a given ray.
	 * Returns Shape.NO_INTERSECTION if the ray doesn't intersect this shape,
	 * otherwise returns a positive value 'd' such that the point (ray.start + d*ray.direction) is on this shape.
	 */
	public abstract double getIntersection(Ray ray);
	
	/**
	 * Gets a point on the surface of the shape, and returns the normal (perpendicular) vector at the given point.
	 * The returned vector is guaranteed to be normalized.
	 */
	public abstract Vector3D getSurfaceNormal(Vector3D surfacePoint);
	
	/**
	 * Gets a ray and reflects it against this shape's surface.
	 * @param ray: Ray starting at a point on the shape, directed 'into' the shape.
	 */
	public Ray getReflectionRay(Ray ray) {
		Vector3D N = this.getSurfaceNormal(ray.getPoint());
		double proj = ray.getDirection().dot(N);
		Vector3D reflectionDir = ray.getDirection().sub(N.scale(2*proj));
		return new Ray(ray.getPoint(), reflectionDir);
	}
	
	public void verifyInitialization() throws RayTracer.RayTracerException {
		if (this.material == null) {
			throw new RayTracer.RayTracerException("Initialization Error: undefined material with id: " + this.materialId);
		}
	}

	public int getMaterialId() {
		return this.materialId;
	}
}
