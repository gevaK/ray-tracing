package RayTracing;

/*
 * a class used to hold an intersection point, and four bounding planes.
 * comparable by the distance of the intersection point on the ray.
 */
public class SortableBoxEntry implements Comparable<SortableBoxEntry>{
	public InfPlane i1, i2, j1, j2, p;
	public double point;
	
	// useful for debugging
	public int index;	
	/*
	 * args:
	 * point: the distance of a source point to p
	 * p : a plane p
	 * i1, i2: a pair of bounding planes
	 * j1, j2: another pair of bounding planes
	 */
	public SortableBoxEntry(double point, InfPlane p, InfPlane i1, InfPlane i2, InfPlane j1, InfPlane j2, int index){
		this.p = p;
		this.i1 = i1;
		this.i2 = i2;
		this.j1 = j1;
		this.j2 = j2;
		this.point = point;
		this.index = index;
	}
	
	public SortableBoxEntry(double point, InfPlane p, InfPlane i1, InfPlane i2, InfPlane j1, InfPlane j2){
		this(point, p, i1, i2, j1, j2, 0);
	}

	public int compareTo(SortableBoxEntry s) {
		double res = this.point - s.point;
		if (Utils.isZero(res))
			return 0;
		else if (res > 0)
			return 1;
		else
			return -1;					
	}
	
	@Override
	public String toString() {
		return "SortableBoxEntry [point=" + point + ", index = " + index 
				+ ",\np=" + p
				+ ",\ni1=" + i1
				+ ",\ni2=" + i2
				+ ",\nj1=" + j1
				+ ",\nj2=" + j2 + "]";
	}
	
}