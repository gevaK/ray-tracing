package RayTracing;

import java.util.ArrayList;

public class Ray {
	
	private Vector3D startPoint;
	private Vector3D direction;
	
	/**
	 * Returns a ray of length zero starting at the origin.
	 */
	public Ray() {
		this(Vector3D.ZERO, Vector3D.ZERO);
	}
	
	/**
	 * Copy constructor.
	 */
	public Ray(Ray r) {
		this.startPoint = r.startPoint;
		this.direction = r.direction;
	}
	
	/**
	 * Construct a ray from a starting point and a direction.
	 */
	public Ray(Vector3D startPoint, Vector3D direction) {
		this.startPoint = startPoint;
		this.direction = direction;
	}
	
	public Vector3D getDirection() {
		return this.direction;
	}
	
	public Vector3D getPoint() {
		return this.startPoint;
	}
	
	public Vector3D getPointAtOffset(double scalar){
		return this.startPoint.add(this.direction.scale(scalar));
	}
	
	/**
	 * Checks intersection of a segment along this ray, defined from this.start to this.start + this.direction.
	 * Returns the fraction of light that goes through all shapes (in range [0, 1]), according to transparency.
	 * @param shapes The list of shapes.
	 */
	private double INTERSECTION_IGNORE_EPSILON = 0.0001;
	public double getPassthroughFraction(ArrayList<Shape> shapes) {
		double fraction = 1;
		for (Shape shape : shapes) {
			// We ignore intersections that are too close to the end of the segment because 
			// the endpoint itself is likely an intersection point and we don't want it to cast shadows on itself
			if (shape.getIntersection(this) < 1 - INTERSECTION_IGNORE_EPSILON) { 
				// We have an intersection. The fraction of the light that goes through the object is determined by its transparency
				fraction *= shape.getMaterial().getTransparency();
				if (Utils.isZero(fraction)) // Exit the loop if we already reached zero
					return 0;
			}
		}
		return fraction;
	}
	
	/**
	 * Returns a new ray, starting at this.start + d * this.direction,
	 * with the same direction as this ray.
	 */
	public Ray advanceStartPoint(double d) {
		return new Ray(this.getPointAtOffset(d), this.direction);
	}

	@Override
	public String toString() {
		return "Ray [startPoint=" + startPoint + ", direction=" + direction
				+ "]";
	}
	
}
