package RayTracing;

import java.util.ArrayList;
import java.util.Random;

public class Light {
	private Vector3D position;
	private Color color;
	private double specularIntensity;
	private double shadowIntensity;
	private double radius;
	private Random rand;
	
	public Light() {
		this.position = null;
		this.color = null;
		this.specularIntensity = Scene.VALUE_UNINITIALIZED;
		this.shadowIntensity = Scene.VALUE_UNINITIALIZED;
		this.radius = Scene.VALUE_UNINITIALIZED;
		this.rand = new Random();
	}
	
	public Vector3D getPosition() {
		return position;
	}
	public void setPosition(Vector3D position) {
		this.position = position;
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public double getSpecularIntensity() {
		return specularIntensity;
	}
	
	public void setSpecularIntensity(double specularIntensity) {
		this.specularIntensity = specularIntensity;
	}
	
	public double getShadowIntensity() {
		return shadowIntensity;
	}
	
	public void setShadowIntensity(double shadowIntensity) {
		this.shadowIntensity = shadowIntensity;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	/**
	 * Returns an estimation of how much light from this light source gets to the given point (in range [0,1]).
	 * The estimation is based on sending numShadowRays^2 light rays from a plane around the light source, and checking how many reach the given point.
	 * Used for shadows.
	 */
	public double getPointIllumination(Vector3D hitPoint, ArrayList<Shape> shapes, int numShadowRays) {
		if (numShadowRays == 1) { // Hard shadows
			Ray lightRay = new Ray(this.position, hitPoint.sub(this.position));
			double lightPercent = Math.max(lightRay.getPassthroughFraction(shapes), 1 - this.shadowIntensity);
			return lightPercent;
		}
		
		// Soft shadows
		// Find orthogonal plane
		Ray lightRay = new Ray(this.position, hitPoint.sub(this.position));
		Vector3D lightDir = lightRay.getDirection();
		// Find two orthogonal vectors, spanning the orthogonal plane
		Vector3D perp1 = lightDir.getOrthogonalVector().normalize();
		Vector3D perp2 = lightDir.cross(perp1).normalize();
		assert Utils.isZero(perp1.dot(lightDir));
		assert Utils.isZero(perp2.dot(lightDir));
		assert Utils.isZero(perp1.dot(perp2));
		Vector3D planeBotLeft = this.position.sub(perp1.scale(this.radius / 2)).sub(perp2.scale(this.radius / 2)); 
		double sideLength = this.radius / numShadowRays;
		Vector3D cellSideX = perp1.scale(sideLength);
		Vector3D cellSideY = perp2.scale(sideLength);
		double accumulatedLight = 0;
		for (int xPos = 0; xPos < numShadowRays; xPos++) {
			for (int yPos = 0; yPos < numShadowRays; yPos++) {
				Vector3D cellBotLeft = planeBotLeft.add(cellSideX.scale(xPos)).add(cellSideY.scale(yPos));
				// Select random point in the cell
				Vector3D lightXYposition = cellBotLeft.add(cellSideX.scale(rand.nextDouble())).add(cellSideY.scale(rand.nextDouble()));
				// Create light ray
				lightRay = new Ray(lightXYposition, hitPoint.sub(lightXYposition));
				// Add the amount of light that reaches from this ray
				accumulatedLight += lightRay.getPassthroughFraction(shapes);
			}
		}
		double lightStrength = accumulatedLight / (numShadowRays*numShadowRays);
		return Math.max(lightStrength, 1 - this.shadowIntensity);
	}
	
	/**
	 * Calculates the diffuse color created by this light on a given point hitPoint,
	 * which is located on a given shape hitShape.
	 */
	public Color getPointDiffuseColor(Vector3D hitPoint, Shape hitShape) {
		// Calculate basic diffuse color
		Vector3D hitNormal = hitShape.getSurfaceNormal(hitPoint);
		Color cDiffuse = this.color.multiply(hitShape.getMaterial().getDiffuseColor());
		// Scale color according to light direction
		Vector3D revLightDirection = this.position.sub(hitPoint).normalize();
		// Find angle cosine by dot product
		double cosHitAngle = hitNormal.dot(revLightDirection);
		if (cosHitAngle < 0) // If the light is behind the surface, it doesn't light it
			cosHitAngle = 0; 
		cDiffuse = cDiffuse.scale(cosHitAngle);
		return cDiffuse;
	}
	
	/**
	 * Calculates the specular reflection color of this light on a given point hitPoint,
	 * which is located on a given shape hitShape.
	 * Due to the nature of specular lighting we must also know the camera position.
	 */
	public Color getPointSpecularReflection(Vector3D observerPosition, Vector3D hitPoint, Shape hitShape) {
		//Regular method (no highlight vector)
		Ray lightHitRay = new Ray(hitPoint, hitPoint.sub(this.position));
		Ray lightReflectionRay = hitShape.getReflectionRay(lightHitRay);
		Vector3D lightReflectionDir = lightReflectionRay.getDirection().normalize();
		Vector3D observerDir = observerPosition.sub(hitPoint).normalize();
		// Find angle cosine by dot product
		double cosSpecularAngle = lightReflectionDir.dot(observerDir);
		if (cosSpecularAngle < 0) { 
			cosSpecularAngle = 0;
		}
		// Exponentiate according to phong coefficient
		double phongValue = Math.pow(cosSpecularAngle, hitShape.getMaterial().getPhongCoefficient());
		// Scale by phong value and this light's specular intensity
		Color cSpecular = hitShape.getMaterial().getSpecularColor().scale(phongValue * this.specularIntensity); 
		return cSpecular;
	}
	
	public void verifyInitialization() throws RayTracer.RayTracerException {
		if (this.position == null)
			throw new RayTracer.RayTracerException("Initialization Error: light position uninitialized.");
		if (this.color == null)
			throw new RayTracer.RayTracerException("Initialization Error: light color uninitialized.");
		if (this.specularIntensity == Scene.VALUE_UNINITIALIZED)
			throw new RayTracer.RayTracerException("Initialization Error: light specular intensity uninitialized.");
		if (this.specularIntensity == Scene.VALUE_UNINITIALIZED)
			throw new RayTracer.RayTracerException("Initialization Error: light shadow intensity uninitialized.");
		if (this.specularIntensity == Scene.VALUE_UNINITIALIZED)
			throw new RayTracer.RayTracerException("Initialization Error: light radius uninitialized.");
	}
	
}
