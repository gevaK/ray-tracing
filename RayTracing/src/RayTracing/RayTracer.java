package RayTracing;

import java.awt.Transparency;
import java.awt.color.*;
import java.awt.image.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


import javax.imageio.ImageIO;

/**
 *  Main class for ray tracing exercise.
 */
public class RayTracer {

	public int imageWidth;
	public int imageHeight;
	public Scene scene;

	/**
	 * Runs the ray tracer. Takes scene file, output image file and image size as input.
	 */
	public static void main(String[] args) {

		try {

			RayTracer tracer = new RayTracer();

                        // Default values:
			tracer.imageWidth = 500;
			tracer.imageHeight = 500;

			if (args.length < 2)
				throw new RayTracerException("Not enough arguments provided. Please specify an input scene file and an output image file for rendering.");

			String sceneFileName = args[0];
			String outputFileName = args[1];

			if (args.length > 3)
			{
				tracer.imageWidth = Integer.parseInt(args[2]);
				tracer.imageHeight = Integer.parseInt(args[3]);
			}


			// Parse scene file:
			tracer.parseScene(sceneFileName);

			// Render scene:
			tracer.renderScene(outputFileName);

//		} catch (IOException e) {
//			System.out.println(e.getMessage());
		} catch (RayTracerException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}

	/**
	 * Parses the scene file and creates the scene. Change this function so it generates the required objects.
	 */
	public void parseScene(String sceneFileName) throws IOException, RayTracerException
	{
		FileReader fr = new FileReader(sceneFileName);

		BufferedReader r = new BufferedReader(fr);
		String line = null;
		int lineNum = 0;
		System.out.println("Started parsing scene file " + sceneFileName);

		this.scene = new Scene();

		while ((line = r.readLine()) != null)
		{
			line = line.trim();
			++lineNum;

			if (line.isEmpty() || (line.charAt(0) == '#'))
			{  // This line in the scene file is a comment
				continue;
			}
			else
			{
				String code = line.substring(0, 3).toLowerCase();
				// Split according to white space characters:
				String[] params = line.substring(3).trim().toLowerCase().split("\\s+");

				if (code.equals("cam"))
				{
                    Vector3D position = Utils.parseVector(params, 0);
                    Vector3D lookAt = Utils.parseVector(params, 3);
                    Vector3D upVector = Utils.parseVector(params, 6);
                    double screenDistance = Utils.parseDouble(params, 9);
                    double screenWidth = Utils.parseDouble(params, 10);
                    Camera cam = new Camera();
                    cam.setPose(position, lookAt, upVector);
                    cam.setScreenParams(screenDistance, screenWidth, ((double) this.imageWidth) / this.imageHeight);
                    this.scene.setCamera(cam);
					System.out.println(String.format("Parsed camera parameters (line %d)", lineNum));
				}
				else if (code.equals("set"))
				{
					Color bgColor = Utils.parseColor(params, 0);
					int numShadowRays = Utils.parseInt(params, 3);
					int recursionDepth = Utils.parseInt(params, 4);
					scene.setNumShadowRays(numShadowRays);
					scene.setRecursionDepth(recursionDepth);
					scene.setBackgroundColor(bgColor);
					System.out.println(String.format("Parsed general settings (line %d)", lineNum));
				}
				else if (code.equals("mtl"))
				{
                    Color cDiffuse = Utils.parseColor(params, 0);
                    Color cSpecular = Utils.parseColor(params, 3);
                    Color cReflect = Utils.parseColor(params, 6);
                    int phongCoeff = Utils.parseInt(params, 9);
                    double transp = Utils.parseDouble(params, 10);
                    //double incidence = Utils.parseDouble(params, 11);
                    Material mat = new Material();
                    mat.setDiffuseColor(cDiffuse);
                    mat.setSpecularColor(cSpecular);
                    mat.setReflectionColor(cReflect);
                    mat.setPhongCoefficient(phongCoeff);
                    mat.setTransparency(transp);
                    scene.addMaterial(mat);
					System.out.println(String.format("Parsed material (line %d)", lineNum));
				}
				else if (code.equals("sph"))
				{
                    Vector3D center = Utils.parseVector(params, 0);
                    double radius = Utils.parseDouble(params, 3);
                    int matIndex = Utils.parseInt(params, 4);
                    Sphere s = new Sphere(matIndex, center, radius);
                    scene.addShape(s);
					System.out.println(String.format("Parsed sphere (line %d)", lineNum));
				}
				else if (code.equals("pln"))
				{
					Vector3D normal = Utils.parseVector(params, 0);
                    double offset = Utils.parseDouble(params, 3);
                    int matIndex = Utils.parseInt(params, 4);
                    InfPlane p = new InfPlane(matIndex, normal, offset);
                    scene.addShape(p);
					System.out.println(String.format("Parsed plane (line %d)", lineNum));
				}
				else if (code.equals("box"))
				{
					Vector3D center = Utils.parseVector(params, 0);
					double scaleX = Utils.parseDouble(params, 3);
                    double scaleY = Utils.parseDouble(params, 4);
                    double scaleZ = Utils.parseDouble(params, 5);
                    double rotationX = Utils.parseDouble(params, 6);
                    double rotationY = Utils.parseDouble(params, 7);
                    double rotationZ = Utils.parseDouble(params, 8);   
                    int matIndex;           
                    if (params.length <= 9) { // Some inputs didn't have matIndex defined, so we support that case as well
                    	matIndex = 1;   	
                    } else {
                    	matIndex = Utils.parseInt(params, 9); 
                    }
                    Box b = new Box(matIndex, center, scaleX, scaleY, scaleZ, rotationX, rotationY, rotationZ);
                    scene.addShape(b);
					System.out.println(String.format("Parsed box (line %d)", lineNum));
				}
				else if (code.equals("lgt"))
				{
					Vector3D position = Utils.parseVector(params, 0);
					Color cLight = Utils.parseColor(params, 3);
					double iSpecular = Utils.parseDouble(params, 6);
					double iShadow = Utils.parseDouble(params, 7);
					double radius = Utils.parseDouble(params, 8);
					Light l = new Light();
					l.setPosition(position);
					l.setColor(cLight);
					l.setSpecularIntensity(iSpecular);
					l.setShadowIntensity(iShadow);
					l.setRadius(radius);
					scene.addLight(l);
					System.out.println(String.format("Parsed light (line %d)", lineNum));
				}
				else
				{
					System.out.println(String.format("ERROR: Did not recognize object: %s (line %d)", code, lineNum));
				}
			}
		} 
		r.close();
		// Initialize materials for all shapes, to support inputting materials after shapes
		scene.initShapeMaterials();
		
		// Verify that we received all of the input that we were supposed to
		scene.verifyInitialization();
		System.out.println("Finished parsing scene file " + sceneFileName);

	}

	/**
	 * Traces a single ray and returns the color seen from the ray's source point.
	 */
	private double RAY_OFFSET_EPSILON = 0.001;
	public Color traceRay(Ray ray, int recursion_depth) {
		/* Pseudocode:
			traceRay():
				Find closest intersection (hitPoint)
				For each light:
					Shading:
						Diffuse
						Specular
					If light casts shadows:
						Apply soft shadows
				If transparent:
					Trace refraction ray
				If mirror:
					Trace reflected ray
		 */
		Color cOut = scene.getBackgroundColor();
		if (recursion_depth == 0) {
			return cOut;
		}
		double dMin = Double.POSITIVE_INFINITY; 
		Shape hitShape = null;
		Vector3D hitPoint = null;
		for (Shape shape : scene.getShapes()) {
			double d = shape.getIntersection(ray);
			hitPoint = ray.getPoint().add(ray.getDirection().scale(d));
			// Notice that we handle a shape as if only one side of it is seen by the camera - the front side. 
			// To do this we ignore hits that are directed outside of the shape (decided by the normal vector).
			// This specifically affects how a transparent box will be seen, how strong the transparency is, makes planes invisible from the one side, etc.
			if (d < dMin) {
				if (shape.getSurfaceNormal(hitPoint).dot(ray.getDirection()) <= 0) { // We ignore any intersection directed inside->out 
					dMin = d;
					hitShape = shape;
				}
			}
		}
		if (hitShape == null) { // No intersection means the color seen is the background color
			return cOut;
		} else {
			// We found an intersection			
			hitPoint = ray.getPoint().add(ray.getDirection().scale(dMin));			
			// Calculate basic shade of the material (diffuse + specular)
			cOut = Color.BLACK;
			for (Light light : scene.getLights()) {	// Go over all lights
				// Add light diffuse color
				Color cDiffuse = light.getPointDiffuseColor(hitPoint, hitShape);

				// Add light specular color
				Color cSpecular = light.getPointSpecularReflection(ray.getPoint(), hitPoint, hitShape);  
				Color cShade = cDiffuse.add(cSpecular);
				
				// Calculate soft shadows
				double lightPercent = 1;
				if (light.getShadowIntensity() > 0) { // If this light casts a shadow, calculate it
					lightPercent = light.getPointIllumination(hitPoint, scene.getShapes(), scene.getNumShadowRays());
				}
				cShade = cShade.scale(lightPercent);
				
				cOut = cOut.add(cShade);
			}
			 
			// Handle transparency
			if (hitShape.getMaterial().isTransparent()) {
				Ray continueRay = ray.advanceStartPoint(dMin + RAY_OFFSET_EPSILON);
				Color cBehind = this.traceRay(continueRay, recursion_depth - 1);
				// Weighted average according to transparency
				cBehind = cBehind.scale(hitShape.getMaterial().getTransparency());
				cOut = cOut.scale(1 - hitShape.getMaterial().getTransparency());
				cOut = cBehind.add(cOut);
			}
			
			// Handle reflections
			if (hitShape.getMaterial().isReflective()) {
				Ray reflectionRay = hitShape.getReflectionRay(ray.advanceStartPoint(dMin));
				reflectionRay = reflectionRay.advanceStartPoint(RAY_OFFSET_EPSILON);
				Color cReflect = this.traceRay(reflectionRay, recursion_depth - 1);
				cReflect = cReflect.multiply(hitShape.getMaterial().getReflectionColor());
				cOut = cOut.add(cReflect);
			}
		}
		return cOut;
	}
	
	/**
	 * Renders the loaded scene and saves it to the specified file location.
	 */
	public void renderScene(String outputFileName)
	{
		long startTime = System.currentTimeMillis();

		// Create a byte array to hold the pixel data:
		byte[] rgbData = new byte[this.imageWidth * this.imageHeight * 3];
        // Write pixel color values in RGB format to rgbData:
        // Pixel [x, y] red component is in rgbData[(y * this.imageWidth + x) * 3]
        //            green component is in rgbData[(y * this.imageWidth + x) * 3 + 1]
        //             blue component is in rgbData[(y * this.imageWidth + x) * 3 + 2]
        //
        // Each of the red, green and blue components should be a byte, i.e. 0-255
		for (int x=0; x < this.imageWidth; x++) {
			for (int y=0; y < this.imageHeight; y++) {
				// Create ray to screen at co-ord (x,y)
				// Notice that we reverse both X and Y axis, because screen co-ords are reversed relative to ours - 
				// In screen co-ords high 'Y' value means down, and in ours it means up. Also in screen co-ords X axis goes right and in ours it goes left according to right hand rule.
				Ray ray = this.scene.getCamera().getRayToScreenPosition(1 - x / (double) this.imageWidth, 1 - y / (double) this.imageHeight);
				// Find pixel color by ray tracing
				Color pixelColor = traceRay(ray, this.scene.getRecursionDepth());
				// Insert color to rgbData array
				rgbData[(y * this.imageWidth + x) * 3] = (byte) (pixelColor.getRed() * 255);
				rgbData[(y * this.imageWidth + x) * 3 + 1] = (byte) (pixelColor.getGreen() * 255);
				rgbData[(y * this.imageWidth + x) * 3 + 2] = (byte) (pixelColor.getBlue() * 255);
			}
		}
		
		long endTime = System.currentTimeMillis();
		Long renderTime = endTime - startTime;

        // The time is measured for your own conveniece, rendering speed will not affect your score
        // unless it is exceptionally slow (more than a couple of minutes)
		System.out.println("Finished rendering scene in " + renderTime.toString() + " milliseconds.");

        // This is already implemented, and should work without adding any code.
		saveImage(this.imageWidth, rgbData, outputFileName);

		System.out.println("Saved file " + outputFileName);

	}




	//////////////////////// FUNCTIONS TO SAVE IMAGES IN PNG FORMAT //////////////////////////////////////////

	/*
	 * Saves RGB data as an image in png format to the specified location.
	 */
	public static void saveImage(int width, byte[] rgbData, String fileName)
	{
		try {

			BufferedImage image = bytes2RGB(width, rgbData);
			ImageIO.write(image, "png", new File(fileName));

		} catch (IOException e) {
			System.out.println("ERROR SAVING FILE: " + e.getMessage());
		}

	}

	/*
	 * Producing a BufferedImage that can be saved as png from a byte array of RGB values.
	 */
	public static BufferedImage bytes2RGB(int width, byte[] buffer) {
	    int height = buffer.length / width / 3;
	    ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_sRGB);
	    ColorModel cm = new ComponentColorModel(cs, false, false,
	            Transparency.OPAQUE, DataBuffer.TYPE_BYTE);
	    SampleModel sm = cm.createCompatibleSampleModel(width, height);
	    DataBufferByte db = new DataBufferByte(buffer, width * height);
	    WritableRaster raster = Raster.createWritableRaster(sm, db, null);
	    BufferedImage result = new BufferedImage(cm, raster, false, null);

	    return result;
	}

	public static class RayTracerException extends Exception {
		public RayTracerException(String msg) {  super(msg); }
	}


}
