package RayTracing;

import java.util.ArrayList;

import RayTracing.RayTracer.RayTracerException;

public class Scene {
	
	public static int VALUE_UNINITIALIZED = -1;
	
	private ArrayList<Material> materials;
	private Camera camera;
	private ArrayList<Light> lights;
	private ArrayList<Shape> shapes;
	// General scene settings
	private Color bgColor;
	private int numShadowRays;
	private int recursionDepth;
	
	public Scene() { 
		this.materials = new ArrayList<Material>();
		this.lights = new ArrayList<Light>();
		this.shapes = new ArrayList<Shape>();
		this.recursionDepth = VALUE_UNINITIALIZED;
		this.numShadowRays = VALUE_UNINITIALIZED;
		this.bgColor = null;
	}
	
	public ArrayList<Light> getLights() {
		return this.lights;
	}

	public void addLight(Light l) {
		this.lights.add(l);
	}
	
	public void addMaterial(Material m) {
		this.materials.add(m);
	}
	
	public ArrayList<Shape> getShapes() {
		return shapes;
	}

	public void addShape(Shape s) {
		this.shapes.add(s);
	}
	
	public Camera getCamera() {
		return camera;
	}
	
	public void setCamera(Camera camera) {
		this.camera = camera;
	}
	
	public Color getBackgroundColor() {
		return bgColor;
	}
	
	public void setBackgroundColor(Color bgColor) {
		this.bgColor = bgColor;
	}
	
	public int getNumShadowRays() {
		return numShadowRays;
	}
	
	public void setNumShadowRays(int numShadowRays) {
		this.numShadowRays = numShadowRays;
	}
	
	public int getRecursionDepth() {
		return recursionDepth;
	}
	
	public void setRecursionDepth(int recursionDepth) {
		this.recursionDepth = recursionDepth;
	}
	
	/**
	 * Gets a one-based index. Returns the material object with the given index.
	 * @throws Exception 
	 */
	public Material getMaterialAtIndex(int index) throws RayTracer.RayTracerException {
		// Notice that the indices in the input files are one-based
		if (index < 1 || index > materials.size()) {
			throw new RayTracer.RayTracerException("ERROR: Material index out of range. Invalid index: " + index);
		}
		return materials.get(index - 1);
	}
	
	/**
	 * Makes sure this object was initialized properly.
	 * @throws RayTracer.RayTracerException if we weren't initialized properly. The error message describes the reason.
	 */
	public void verifyInitialization() throws RayTracer.RayTracerException {
		if (this.bgColor == null)
			throw new RayTracer.RayTracerException("Initialization Error: background color was not initialized.");
		if (this.numShadowRays == VALUE_UNINITIALIZED)
			throw new RayTracer.RayTracerException("Initialization Error: number of shadow rays was not initialized.");
		if (this.recursionDepth == VALUE_UNINITIALIZED)
			throw new RayTracer.RayTracerException("Initialization Error: recursion depth was not initialized.");
		if (this.camera == null) 
			throw new RayTracer.RayTracerException("Initialization Error: camera was not initialized.");
		else {
			this.camera.verifyInitialization();
		}
		for (Light lgt : this.lights) {
			lgt.verifyInitialization();
		}
		for (Material mat : this.materials) {
			mat.verifyInitialization();
		}
		for (Shape s : this.shapes) {
			s.verifyInitialization();
		}
	}

	public void initShapeMaterials() throws RayTracerException {
		for (Shape s : this.shapes) {
			s.setMaterial(this.getMaterialAtIndex(s.getMaterialId()));
		}
	}
} 
