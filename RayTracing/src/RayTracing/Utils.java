package RayTracing;


public class Utils {
	
	/**
	 * Parses a double value out of an array of strings at a given offset.
	 */
	public static double parseDouble(String[] params, int offset) {
		return Double.parseDouble(params[offset]);
	}
	
	/**
	 * Parses an integer value out of an array of strings at a given offset.
	 */
	public static int parseInt(String[] params, int offset) {
		return Integer.parseInt(params[offset]);
	}
	
	/**
	 * Parses a Vector3D out of an array of strings at a given offset.
	 */
	public static Vector3D parseVector(String[] params, int offset) {
		return new Vector3D(parseDouble(params, offset), parseDouble(params, offset + 1), parseDouble(params, offset + 2));
	}
	
	/**
	 * Parses a Color out of an array of strings at a given offset.
	 */
	public static Color parseColor(String[] params, int offset) {
		double red = parseDouble(params, offset);
		double green = parseDouble(params, offset + 1);
		double blue = parseDouble(params, offset + 2);
		return new Color(red, green, blue);
	}
		
	/**
	 * Checks if a double value is "nearly" zero.
	 * Used to recognize zero values despite floating point errors.
	 */
	private static double FLOAT_ERROR_EPSILON = 0.00001;
	public static boolean isZero(double val) {
		return ( val > -FLOAT_ERROR_EPSILON && val < FLOAT_ERROR_EPSILON);
	}

}
